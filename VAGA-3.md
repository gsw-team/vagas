# # 3 - Líder Técnico
GSW, Full-time em São José dos Campos

A GSW é uma empresa de tecnologia voltada a desenvolvimento e soluções de software. Aqui você encontrará excelentes desafios, e poderá desfrutar do uso de stacks atualizadas e provisionamento de ambientes.

A GSW tem trabalhado para mudar o ambiente de desenvolvimento e stacks dos projetos da região, trazendo inovações que estão em alta no mercado como: AWS, Heroku, Docker, Vagrant, Chef, Puppet, Ansible, AngularJS, Backbone, integrações com Slack, etc.

#### Requisitos
* Conhecimento em containers (WebLogic e/ou JBoss);
* Conhecimento de configurações JDNI, JMS, Datasource, cache, etc;
* Conhecimento de arquiteturas microservices e RESTFul.

A vaga de liderança técnica requer habilidades não só técnicas, mas também:
* Boa comunicação;
* Desenvoltura para negociações.

O candidato irá atuar na concepção de novos projetos, auxiliando na montagem da arquitetura e sugerindo stacks padrões para os projetos.

Também auxiliando na padronização de produtos atuais.

O grande desafio da vaga é auxiliar na definição de um ambiente de desenvolvimento padrão, com reaproveitamento de serviços (microservices) , padrões de desenvolvimento e qualidade.

#### Stack do projeto
* Java;
* AngularJS;
* JBoss;
* WebLogic (desejo de tornar padrão).

#### Diferenciais
* DevOps;
* Atualizado nas novas stacks do mercado;
* Github;
* Integração contínua.
