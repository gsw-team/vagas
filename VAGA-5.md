# # 5 - Desenvolvedor Full Stack - Java, e AngularJS

GSW, Full-time em São José dos Campos

A GSW é uma empresa de tecnologia voltada a desenvolvimento e soluções de software. Aqui você encontrará excelentes desafios, e poderá desfrutar do uso de stacks atualizadas e provisionamento de ambientes.

A GSW tem trabalhado para mudar o ambiente de desenvolvimento e stacks dos projetos da região, trazendo inovações que estão em alta no mercado como: AWS, Heroku, Docker, Vagrant, Chef, Puppet, Ansible, AngularJS, Backbone, integrações com Slack, etc.

#### Requisitos
Caso não conheça todos os itens contidos na stack do projeto, que tenha pro atividade e vontade em aprender.

Clean code, qualidade de código (testes unitários, TDD, etc), e boas práticas.

#### Stack do projeto
* Java 7 ou superior;
* Spring MVC;
* Spring Data;
* Spring Boot;
* Spring Security;
* Selenium;
* Bower;
* Gulp;
* AngularJS;
* Jasmine;
* Liquibase;
* Hibernate;
* Hibernate Envers;
* SASS e CSS3;
* HTML 5.

#### Infraestrutura
* Apache ou NGInx;
* Weblogic 11c ou superior;
* MongoDB;
* Oracle Database;
* RabbitMQ ou IronMQ;
* Servidor de versionamento GIT;
* Jenkins ou afins (preferência por cloud);
* Nexus;
* Sonar;
* JIRA ou Redmine;
* Testlink.

#### Diferenciais
* DevOps;
* Atualizado nas novas stacks do mercado;
* Github;
* Integração contínua.
