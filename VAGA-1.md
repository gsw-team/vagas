# # 1 - Desenvolvedor Full Stack - Java, Groovy e AngularJS

GSW, Full-time em São José dos Campos

A GSW é uma empresa de tecnologia voltada a desenvolvimento e soluções de software. Aqui você encontrará excelentes desafios, e poderá desfrutar do uso de stacks atualizadas e provisionamento de ambientes.

A GSW tem trabalhado para mudar o ambiente de desenvolvimento e stacks dos projetos da região, trazendo inovações que estão em alta no mercado como: AWS, Heroku, Docker, Vagrant, Chef, Puppet, Ansible, AngularJS, Backbone, integrações com Slack, etc.

#### Requisitos
Conhecimento em SCRUM.

Caso não conheça todos os itens contidos na stack do projeto, que tenha pro atividade e vontade em aprender.

Clean code, qualidade de código (testes unitários, TDD, etc), e boas práticas.

#### Stack do projeto

##### Frontend
* Javascript;
* AngularJS;
* Jasmine;
* Karma;
* Gulp.

##### Backend
* Spring Framework;
* Spring Boot;
* Spring Web MVC;
* Spring Data;
* Spring Security;
* Spring HATEOAS;
* Spring Web Services;
* Spring LDAP;
* Maven;
* Hibernate;
* Hibernate Validator;
* JUnit;
* Mockito;
* Spock;
* Geb.
