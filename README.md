# Vagas GSW

Para se candidatar a uma ou mais vaga, basta enviar e-mail para: **candidato@gsw.com.br**.

*Favor especificar o ID (#) da vaga.*

#### Disponíveis:
* [# 1 - Desenvolvedor Full Stack - Java, Groovy e AngularJS - 1 Vaga](VAGA-1.md)
* [# 4 - Desenvolvedor Full Stack - Java, e AngularJS - 2 Vagas](VAGA-4.md)
* [# 5 - Desenvolvedor Full Stack - Java, e AngularJS - 2 Vagas](VAGA-5.md)
* `#` 6 - Desenvolvedor iOS - 1 Vaga
* `#` 7 - Desenvolvedor Node.js e Backbone.js - 1 Vaga
#### Não disponíveis:
* [# 2 - Desenvolvedor Full Stack - Java, e AngularJS - 1 Vaga](VAGA-2.md) - **PREENCHIDA**
* [# 3 - Líder Técnico - 2 Vagas](VAGA-3.md) - **CONGELADA**