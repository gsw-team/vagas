# # 4 - Desenvolvedor Full Stack - Java, e AngularJS

GSW, Full-time em São José dos Campos

A GSW é uma empresa de tecnologia voltada a desenvolvimento e soluções de software. Aqui você encontrará excelentes desafios, e poderá desfrutar do uso de stacks atualizadas e provisionamento de ambientes.

A GSW tem trabalhado para mudar o ambiente de desenvolvimento e stacks dos projetos da região, trazendo inovações que estão em alta no mercado como: AWS, Heroku, Docker, Vagrant, Chef, Puppet, Ansible, AngularJS, Backbone, integrações com Slack, etc.

#### Requisitos
Caso não conheça todos os itens contidos na stack do projeto, que tenha pro atividade e vontade em aprender.

Clean code, qualidade de código (testes unitários, TDD, etc), e boas práticas.

#### Stack do projeto
* Spring Web MVC;
* Spring Data;
* Spring Framework;
* Spring Security;
* Spring LDAP;
* Spring Events;
* Scheduler;
* JMS;
* SOAP;
* Banco de dados Oracle;
* Maven / Nexus;
* Integração Contínua com Jenkins;
* Hibernate;
* CSS 3;
* AngularJS;
* Jasmine;
* SVN ou GIT;
* Servidor WebLogic;
* Java 7;

> O candidato não precisa conhecer a stack completa, porém, alguns itens são importantes como: REST, Spring, JPA e Javascript.

#### Diferenciais
* DevOps;
* Atualizado nas novas stacks do mercado;
* Github;
* Integração contínua.
